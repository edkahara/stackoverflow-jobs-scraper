import requests
from bs4 import BeautifulSoup as bs
jobs_page = 'https://stackoverflow.com/jobs?c=usd&mxs=Junior&sort=p'
page = requests.get(jobs_page)
soup = bs(page.content, 'html.parser')
jobs_list = soup.find('div', attrs={'class': 'listResults'}).find_all('div', {'class': '-item'})
with open('test.html','wb') as f:
    for job in jobs_list:
        f.write(job.prettify().encode("utf-8"))
    f.close()
